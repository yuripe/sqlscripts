DROP DATABASE IF EXISTS ModernWays;
CREATE DATABASE ModernWays;
CREATE table huisdieren(
	Naam varchar(100) char set utf8mb4,
    Leeftijd smallint unsigned,
    Soort varchar(50)
);
alter table huisdieren add column baasje varchar(100) char set utf8mb4;